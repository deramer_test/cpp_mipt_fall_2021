#include <iostream>

namespace ns {
class A {
public:
    explicit A(int n, int a = 5) : n_(n + 1) {
        std::cout << "ctor called" << std::endl;
    }

    friend A operator+(A lhs, const A& rhs);

    friend A operator+(A lhs, int rhs) {
        lhs.n_ += rhs;
        return lhs;
    }

    friend A operator+(int lhs, A rhs) {
        rhs.n_ += lhs;
        return rhs;
    }

    int Get() const {
        return n_;
    }

private:
    int n_;
};

A operator+(A lhs, const A& rhs) {
    lhs.n_ += rhs.n_;
    return lhs;
};

class B {
public:
    explicit B(int n) : n_(n) {};

    explicit operator A() {
        return A(n_);
    };

private:
    int n_;
};

int APlusOne(A a) {
    return a.Get() + 1;
};

int IntPlusOne(int n) {
    return n + 1;
};
}; // ns

int main() {
    ns::A a(5);
    ns::B b(5), b2(6);
    int n = 5;
    std::cout << (5 + a).Get() << std::endl;
    std::cout << APlusOne(a) << std::endl;
    // std::cout << APlusOne(b) << std::endl;
    std::cout << ns::IntPlusOne(n) << std::endl;
    ns::A c(b);
    std::cout << c.Get() << std::endl;
    ns::A a1 = static_cast<ns::A>(b), a2 = static_cast<ns::A>(b2);
    std::cout << (a1 + a2).Get() << std::endl;

    return 0;
}
