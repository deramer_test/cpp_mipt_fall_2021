#include <iostream>
#include <vector>
#include <algorithm>

int main() {
    std::vector<int> vec{1, -1, 5, 10, -15, -11};

    for (int i = 0; i < static_cast<int>(vec.size()); ++i) {
        std::cout << vec[i] << ' ';
    }
    std::cout << std::endl;

    // iterators
    for (auto it = vec.begin(); it != vec.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    // range-based for
    // implicitly calls vec.begin, vec.end; n = *it
    for (auto& n : vec) {
        std::cout << n << ' ';
    }
    std::cout << std::endl;

    // iterators are used almost everywhere in stl
    std::sort(vec.begin(), vec.end());
    for (auto& n : vec) {
        std::cout << n << ' ';
    }
    std::cout << std::endl;

    return 0;
}
